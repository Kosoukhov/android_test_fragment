package com.example.fragmenttest.ui

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.fragmenttest.MainActivity
import com.example.fragmenttest.R
import kotlinx.android.synthetic.main.fragment_second.*

// А вообще, интерфейс SecondFragmentView, наверное, можно было бы назвать универсально...
//Нет, тут интерфейс должен называться ThirdFragmentView и находится в ThirdFragmentViewModel
//Универсально не получится, т.к. он используется для связи между Fragment и ViewModel, и в зависимости
//от требований бизнес-логики, там будут разные функции
class ThirdFragment : Fragment(), SecondFragmentView {
    private lateinit var viewModel: ThirdFragmentViewModel
    private var qClick: Int = 0
    private lateinit var clickListener: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Для получения количества нажатий второго фрагмента из MainActivity
        if (context is MainActivity) {
            clickListener = context
        }
        // Количество нажатий во втором фрагменте, переменная уровня класса
        qClick = clickListener.qClick
        Log.d(TAG, "Количество нажатий $qClick")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    // onActivityCreated(): вызывается после создания activity.
    // С этого момента к компонентам интерфейса можно обращаться через метод findViewById().
    // onStart(): вызывается, когда фрагмент становится видимым.
    // Здесь, вижу, элементы управления начинают жить, кнопке добавляется листнер
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ThirdFragmentViewModel::class.java)
        // Видимо, здесь добавляется подписка/подписчик на изменение во ViewModel
        // не на изменения а на хм.. сообщения из ViewModel в сторону View
        // Получается, объекты этого класса (подписчика) будут взаимодействовать с SecondFragmentViewModel
        // Да, через интерфейс ThirdFragmentView который, как правило, уникален для каждой пары View ViewModel
        viewModel.viewSubscribers.addSubscriber(this)
        viewModel.setup()
        // Передать количество нажатий второго фрагмента в модель
        viewModel.setCounter(qClick)

        // А это подписка на LiveData через observer
        // С лямбдами пока не совсем разобрался, это в планвх на выходные.
        // Но, видимо, он получает то самое число счетчика (единственный параметр, it) и устанавливает его в текстовое поле
        viewModel.counterField.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "counterField.observe")
            Log.d(TAG, "Передано количество нажатий $qClick")
            tv_Counter?.text = it.toString()
        })

        // А тут, похоже, по нажатию кнопки изменяется объект в LiveData
        btn_go?.setOnClickListener {
            Log.d(TAG, "Go clicked!")
            viewModel.counterField.value?.let {
                viewModel.increaseCounter(it)
            }
        }
    }

    // ...вот и функция интерфейса SecondFragmentView. Видимо, ее цель обнулить счетчик при первой загрузке фрагмента
    //Да, тут вообще помещаются любые действия, которые нужно выполнить на старте фрагмента но после того,
    //как произошла инициализация View и ViewModel
    override fun firstSetupView(viewModel: AndroidViewModel) {
        Log.d(TAG, "firstSetupView")
        tv_Counter?.text = "0"
    }

    //Это для дебага в Logcat можно фильтровать вывод строк по ### или ### MainActivity
    companion object {
        const val TAG = "### ThirdFragment"
    }
}