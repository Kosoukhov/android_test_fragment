package com.example.fragmenttest

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import com.example.fragmenttest.ui.FirstFragment
import com.example.fragmenttest.ui.SecondFragment
import com.example.fragmenttest.ui.ThirdFragment
import kotlinx.android.synthetic.main.activity_main.*



class MainActivity : AppCompatActivity(), SecondFragment.Interoperate {

    // Количество нажатий во втором фрагменте
    var qClick:Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tv_First.setOnClickListener {
            Log.d(TAG, "tv_First click!")
            supportFragmentManager.beginTransaction()
                .replace(R.id.contentContainer, FirstFragment()).commit()

        }

        tv_Second.setOnClickListener {
            Log.d(TAG, "tv_Second click!")
            supportFragmentManager.beginTransaction()
                .replace(R.id.contentContainer, SecondFragment()).commit()

        }


        tv_Third.setOnClickListener {
            Log.d(TAG, "tv_Third click!")
            supportFragmentManager.beginTransaction()
                .replace(R.id.contentContainer,
                    ThirdFragment()
                ).commit()

        }

    }

    //Это для дебага в Logcat можно фильтровать вывод строк по ### или ### MainActivity
    companion object {
        const val TAG = "### MainActivity"
    }


    // Реализация интерфейса для работы с количеством нажатий
    override fun quantClick(click: Int) {
        qClick = click
        Log.d(TAG, "Количество нажатий $qClick")
    }

}
