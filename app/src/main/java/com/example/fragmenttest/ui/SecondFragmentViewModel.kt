package com.example.fragmenttest.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.fragmenttest.helpers.mvvm.WeakSubscriberArray

interface SecondFragmentView {
    fun firstSetupView(viewModel: AndroidViewModel)
}

class SecondFragmentViewModel(application: Application) : AndroidViewModel(application) {

    // А это объект класса (управление подписками на LiveData???) который делал Вова))
    val viewSubscribers = WeakSubscriberArray<SecondFragmentView>()

    private var isFirstSetup = true

    // А это LiveData
    // Если на это хранилище кто-то (activity, фрагменты) подписан, то они могут получать объекты
    // из хранилища
    val counterField = MutableLiveData<Int>(0)

    fun increaseCounter(curVal: Int) {
        counterField.value = curVal + 1
    }

    fun setup() {
        if (isFirstSetup) {
            isFirstSetup = false
            firstSetup()
        }
    }

    // А здесь тоже подписка на LiveData? Двусторонняя?
    private fun firstSetup() {
        viewSubscribers.forEachSubscribers { it.firstSetupView(this) }
    }
}