package com.example.fragmenttest.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.fragmenttest.helpers.mvvm.WeakSubscriberArray



class ThirdFragmentViewModel(application: Application) : AndroidViewModel(application) {

    // А это объект класса (управление подписками на LiveData???) который делал Вова))
    //Это сделано для общения между View и ViewModel с учетом жизненного цикла
    //При вызове метода в View из ViewModel проверяется наличие метода (фрагмент то может уже не существовать и все сломается)
    val viewSubscribers = WeakSubscriberArray<SecondFragmentView>()

    private var isFirstSetup = true

    // А это LiveData
    // Если на это хранилище кто-то (activity, фрагменты) подписан, то они могут получать объекты
    // из хранилища
    val counterField = MutableLiveData<Int>(0)

    fun increaseCounter(curVal: Int) {
        counterField.value = curVal + 1
    }

    fun setCounter(quant:Int) {
        counterField.value = quant
    }

    fun setup() {
        if (isFirstSetup) {
            isFirstSetup = false
            firstSetup()
        }
    }

    // А здесь тоже подписка на LiveData? Двусторонняя?
    //Нет это метод инициализии, в firstSetupView помещается бизнес-логика,
    //отработка которой необходима при запуске фрагмента
    //например, установка дефолтных значений или вызов бекенда
    // а вызов через viewSubscribers.forEachSubscribers { it.*} нужен для того, чтобы избежать
    //случая, когда фрагмента и его метода не существует, т.е. когда ViewModel еще существует, а View уже нет
    private fun firstSetup() {
        viewSubscribers.forEachSubscribers { it.firstSetupView(this) }
    }
}