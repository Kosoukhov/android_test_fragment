package com.example.fragmenttest.ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.fragmenttest.MainActivity
import com.example.fragmenttest.R
import kotlinx.android.synthetic.main.fragment_second.*

// Вижу, реализует интерфейс SecondFragmentView, объявленный в файле SecondFragmentViewModel
class SecondFragment : Fragment(), SecondFragmentView {

    // Этот интерфейс будет реализовать в MainActivity
    interface Interoperate {
        // Функция возвращает количество нажатий на кнопку
        fun quantClick(click:Int)
    }

    private lateinit var viewModel: SecondFragmentViewModel
    private lateinit var clickListener: MainActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Чтобы в Activity можно было передать количество нажатий
        if(context is MainActivity) {
            clickListener = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false)
    }

    // onActivityCreated(): вызывается после создания activity.
    // С этого момента к компонентам интерфейса можно обращаться через метод findViewById().
    // onStart(): вызывается, когда фрагмент становится видимым.
    // Здесь, вижу, элементы управления начинают жить, кнопке добавляется листнер
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(SecondFragmentViewModel::class.java)
        // Видимо, здесь добавляется подписка/подписчик на изменение во ViewModel
        // Получается, объекты этого класса (подписчика) будут взаимодействовать с SecondFragmentViewModel
        viewModel.viewSubscribers.addSubscriber(this)
        viewModel.setup()

        // А это подписка на LiveData через observer
        // С лямбдами пока не совсем разобрался, это в планвх на выходные.
        // Но, видимо, он получает то самое число счетчика (единственный параметр, it) и устанавливает его в текстовое поле
        viewModel.counterField.observe(viewLifecycleOwner, Observer {
            Log.d(TAG, "counterField.observe")
            tv_Counter?.text = it.toString()
        })

        // А тут, похоже, по нажатию кнопки изменяется объект в LiveData
        btn_go?.setOnClickListener {
            Log.d(TAG, "Go clicked!")
            viewModel.counterField.value?.let {
                viewModel.increaseCounter(it)
            }
            // Малопонятная функция, но факт - в MainActivity уходит число нажатий
            viewModel.counterField.value?.let { it1 -> clickListener.quantClick(it1) }
        }
    }

    // ...вот и функция интерфейса SecondFragmentView. Видимо, ее цель обнулить счетчик при первой загрузке фрагмента
    override fun firstSetupView(viewModel: AndroidViewModel) {
        Log.d(TAG, "firstSetupView")
        tv_Counter?.text = "0"
    }

    //Это для дебага в Logcat можно фильтровать вывод строк по ### или ### MainActivity
    companion object {
        const val TAG = "### SecondFragment"
    }
}